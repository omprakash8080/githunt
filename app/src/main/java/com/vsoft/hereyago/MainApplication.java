package com.vsoft.hereyago;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.vsoft.hereyago.Utils.TinyDB;
import com.vsoft.hereyago.api.IOCContainer;
import com.vsoft.hereyago.api.IResponseSubscribe;

import java.util.Hashtable;

/**
 * Created by omprakash on 1/3/16.
 */
public class MainApplication extends Application implements IResponseSubscribe {

    public static final String TAG = "AppController";
    private static MainApplication mInstance;
    TinyDB tinyDB;

    @Override
    public void onCreate() {
        super.onCreate();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        mInstance = this;
        tinyDB = new TinyDB(getApplicationContext());

    }

    public static synchronized MainApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }
    }

    @Override
    public void onError(Throwable error, String tag) {

    }

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();
    public static Typeface get(Context c, String name) {
        synchronized (cache) {
            if (!cache.containsKey(name)) {
                Typeface t = Typeface.createFromAsset(c.getAssets(), "fonts/"
                        + name);
                cache.put(name, t);
            }
            return cache.get(name);
        }
    }
}
