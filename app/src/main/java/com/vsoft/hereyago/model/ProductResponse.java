package com.vsoft.hereyago.model;

import java.util.List;

/**
 * Created by om on 1/17/17.
 */

public class ProductResponse {
    private List<Product> products = null;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
