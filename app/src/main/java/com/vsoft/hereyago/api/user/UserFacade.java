package com.vsoft.hereyago.api.user;

import android.content.Context;

import com.vsoft.hereyago.api.APIService;
import com.vsoft.hereyago.api.IResponseSubscribe;
import com.vsoft.hereyago.api.RestClient;

/**
 * Created by omprakash on 8/4/16.
 */
public class UserFacade implements IUserFacade, IResponseSubscribe {

    public static String TAG = "UserFacade";
    private IResponseSubscribe responseObserver;
    APIService apiService;

    public UserFacade(Context context, IResponseSubscribe responseObserver) {
        this.responseObserver = responseObserver;
        apiService = RestClient.getInstance().getApiService(context);
    }

    @Override
    public void onSuccess(Object response, String tag) {
        responseObserver.onSuccess(response, tag);
    }

    @Override
    public void onError(Throwable error, String tag) {
        responseObserver.onError(error,tag);
    }

//    @Override
//    public void getProfile( final String tag) {
//
//       apiService.getProfile(new Callback<ProfileResponse>() {
//
//           @Override
//           public void success(ProfileResponse response, retrofit.client.Response response2) {
//               onSuccess(response, tag);
//           }
//
//           @Override
//           public void failure(RetrofitError error) {
//               onFailure(error, tag);
//           }
//       });
//    }
//
//    @Override
//    public void saveprofileInfo(ProfileInfo profileInfo,final String tag) {
//
//        apiService.saveProfileInfo(profileInfo, new Callback<ProfileSaveResponse>() {
//
//            @Override
//            public void success(ProfileSaveResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void getCountriesByProfile(final String tag) {
//        apiService.getCountryByProfile(new Callback<CountryListResponse>() {
//
//            @Override
//            public void success(CountryListResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void doEverification(Passport passport, final String tag) {
//            apiService.everification(passport, new Callback<PassportDetailsResponse>() {
//
//                @Override
//                public void success(PassportDetailsResponse response, retrofit.client.Response response2) {
//                    onSuccess(response, tag);
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    onFailure(error, tag);
//                }
//            });
//    }
//
//    @Override
//    public void documentUpload(ProfileFileUpload profileFileUpload, final String tag) {
//        apiService.documentUpload(profileFileUpload, new Callback<DocumentUploadResponse>() {
//
//            @Override
//            public void success(DocumentUploadResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void getDocument(final String tag) {
//        apiService.getDocument(new Callback<DocumentResponse>() {
//
//            @Override
//            public void success(DocumentResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void getReferralCode(final String tag) {
//        apiService.getReferralCode(new Callback<ReferralCodeResponse>() {
//
//            @Override
//            public void success(ReferralCodeResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }

    @Override
    public void setTag(String tag) {

    }
}
