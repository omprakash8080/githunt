package com.vsoft.hereyago.api;

public interface IResponseSubscribe {

	 void onSuccess(Object response, String tag);
	 void onError(Throwable error, String tag);


}
