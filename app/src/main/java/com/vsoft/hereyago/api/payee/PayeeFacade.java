package com.vsoft.hereyago.api.payee;

import android.content.Context;

import com.vsoft.hereyago.api.APIService;
import com.vsoft.hereyago.api.IResponseSubscribe;
import com.vsoft.hereyago.api.RestClient;


/**
 * Created by omprakash on 4/3/16.
 */
public class PayeeFacade implements IPayeeFacade, IResponseSubscribe {

    public static String TAG = "PayeeFacade";
    private IResponseSubscribe responseObserver;
    APIService apiService;

    public PayeeFacade(Context context, IResponseSubscribe responseObserver) {
        this.responseObserver = responseObserver;
        apiService = RestClient.getInstance().getApiService(context);
    }

    @Override
    public void onSuccess(Object response, String tag) {
        responseObserver.onSuccess(response, tag);
    }

    @Override
    public void onError(Throwable error, String tag) {
        responseObserver.onError(error, tag);
    }

//    @Override
//    public void listPayee(final String tag) {
//        apiService.payeeList(new Callback<PayeeListResponse>() {
//
//            @Override
//            public void success(PayeeListResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void addPayee(PayeeListView payeeListView, final String tag) {
//        apiService.addPayee(payeeListView, new Callback<AddPayeeResponse>() {
//
//            @Override
//            public void success(AddPayeeResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void deletePayee(String payeeTemporaryId, final String tag) {
//        apiService.deletePayee(payeeTemporaryId, new Callback<PayeeDeleteResponse>() {
//
//            @Override
//            public void success(PayeeDeleteResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void detailsPayee(String payeeTemporaryId, final String tag) {
//        apiService.detailsPayee(payeeTemporaryId, new Callback<PayeeDetailResponse>() {
//
//            @Override
//            public void success(PayeeDetailResponse
//                                        response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void checkDuplicatePayee(PayeeListView payeeListView, final String tag) {
//        apiService.checkduplicatePayee(payeeListView, new Callback<DuplicatePayeeResponse>() {
//
//            @Override
//            public void success(DuplicatePayeeResponse
//                                        response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//
//    @Override
//    public void submitPayeeSelection(PayeeSelectionEntity selectionEntity, final String tag) {
//        apiService.submitPayeeSelection(selectionEntity, new Callback<SubmitPayeeSelectionResponse>() {
//
//            @Override
//            public void success(SubmitPayeeSelectionResponse
//                                        response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }

    @Override
    public void setTag(String tag) {

    }
}