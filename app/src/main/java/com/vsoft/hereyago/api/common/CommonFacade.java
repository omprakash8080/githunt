package com.vsoft.hereyago.api.common;

import android.content.Context;

import com.vsoft.hereyago.Utils.TinyDB;
import com.vsoft.hereyago.api.APIService;
import com.vsoft.hereyago.api.IResponseSubscribe;
import com.vsoft.hereyago.api.RestClient;

/**
 * Created by omprakash on 12/4/16.
 */
public class CommonFacade implements ICommonFacade, IResponseSubscribe {

    public static String TAG = "CommonFacade";
    private IResponseSubscribe responseObserver;
    APIService apiService;
    TinyDB tinyDB;

    public CommonFacade(Context context, IResponseSubscribe responseObserver) {
        this.responseObserver = responseObserver;
        apiService = RestClient.getInstance().getApiService(context);
        if(context != null){
            tinyDB = new TinyDB(context);
        }

    }

    @Override
    public void onSuccess(Object response, String tag) {
        responseObserver.onSuccess(response, tag);
    }

    @Override
    public void onError(Throwable error, String tag) {

    }


//    @Override
//    public void getCountry(final String tag) {
//        apiService.getCountry(new Callback<CountryResponse>() {
//
//            @Override
//            public void success(CountryResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void getCountryInfo(String countryId, final String tag) {
//        apiService.getCountryInfo(countryId, new Callback<CountryInfoResponse>() {
//
//            @Override
//            public void success(CountryInfoResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//    }
//
//    @Override
//    public void getRecipientCountry(String selectedCountry, final String tag) {
//        apiService.getRecipientCountry(selectedCountry, new Callback<RecipientCountryResponse>() {
//
//            @Override
//            public void success(RecipientCountryResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void isRecipientCountry(final String tag) {
//        apiService.isRecipientCountry(new Callback<Response>() {
//
//            @Override
//            public void success(Response response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void getFxRate(Map<String, String> params, final String tag) {
//        apiService.getFxRate(params, new Callback<Response>() {
//
//            @Override
//            public void success(Response response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void getAmountFee(final String tag) {
//        apiService.getAmountFee(new Callback<Response>() {
//
//            @Override
//            public void success(Response response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void validateDate(final String tag) {
//        apiService.validateDate(new Callback<Response>() {
//
//            @Override
//            public void success(Response response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//    @Override
//    public void getPurposeCode(final String tag) {
//        apiService.getPurposeCode(new Callback<GetPurposeCodeResponse>() {
//
//            @Override
//            public void success(GetPurposeCodeResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void getBankDetails(final String tag) {
//        apiService.getBankDetails(new Callback<Response>() {
//
//            @Override
//            public void success(Response response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//    @Override
//    public void getBankList(final String tag) {
//        apiService.getBankList(new Callback<Response>() {
//
//            @Override
//            public void success(Response response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//    @Override
//    public void getAustraliaPostalCodeList(String pincode, final String tag) {
//        apiService.getAustraliaPostalCodeList(pincode, new Callback<AustraliaPostalCodeResponse>() {
//
//            @Override
//            public void success(AustraliaPostalCodeResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//    @Override
//    public void getSourceOfFund(final String tag) {
//        apiService.getSourceOfFund(new Callback<SourceOfFundResponse>() {
//
//            @Override
//            public void success(SourceOfFundResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//                tinyDB.putObject("SourceOfFundResponse", response);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void getPostalCodeList(String prefix, final String tag) {
//        apiService.getPostalCodeList(prefix, new Callback <PostalCodeListResponse> () {
//
//            @Override
//            public void success (PostalCodeListResponse response, retrofit.client.Response response2){
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure (RetrofitError error){
//                onFailure(error, tag);
//            }
//        });
//
//    }
//
//    @Override
//    public void getIFSCCodeList(String prefix, String countryCode, final String tag) {
//        apiService.getIFSCCodeList(prefix, countryCode, new Callback<IFSCCodeListResponse>() {
//
//            @Override
//            public void success(IFSCCodeListResponse response, retrofit.client.Response response2) {
//                onSuccess(response, tag);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                onFailure(error, tag);
//            }
//        });
//
//    }

    @Override
    public void setTag(String tag) {

    }
}