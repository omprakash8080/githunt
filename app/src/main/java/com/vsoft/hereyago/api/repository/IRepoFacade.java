package com.vsoft.hereyago.api.repository;

import com.vsoft.hereyago.api.IBaseFacade;
import com.vsoft.hereyago.api.IResponseSubscribe;

/**
 * Created by omprakash on 18/4/16.
 */
public interface IRepoFacade extends IBaseFacade, IResponseSubscribe {

    void getRepos1(final String tag);
    void getRepos2(final String tag);

    void getRepos(String startDate, String endDate, String lang, final String tag);
    void getReposAndroid(String query, String lang, final String tag);
//
//    void verifyAccessToPayment(String tag);
//    void calculatePayment(Map<String, String> params, String tag);

}
