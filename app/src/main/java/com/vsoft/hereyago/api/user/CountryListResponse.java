package com.vsoft.hereyago.api.user;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omprakash on 12/4/16.
 */
public class CountryListResponse {
    private Boolean isSuccess;
    private Object error;
    private List<List<String>> responseData = new ArrayList<>();
    private Object redirectionURL;
    private Object authToken;

    public Boolean isSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public List<List<String>> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<List<String>> responseData) {
        this.responseData = responseData;
    }

    public Object getRedirectionURL() {
        return redirectionURL;
    }

    public void setRedirectionURL(Object redirectionURL) {
        this.redirectionURL = redirectionURL;
    }

    public Object getAuthToken() {
        return authToken;
    }

    public void setAuthToken(Object authToken) {
        this.authToken = authToken;
    }
}
