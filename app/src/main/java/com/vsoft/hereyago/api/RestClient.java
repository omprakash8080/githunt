package com.vsoft.hereyago.api;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vsoft.hereyago.common.InstaremSharedPreferences;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by omprakash on 30/03/16.
 */
public class RestClient {

    Retrofit retrofit;
    private static RestClient client;
    private APIService apiService;
    InstaremSharedPreferences sp;

    public static final String BASE_URL = "https://api.github.com/";

    private RestClient() {
//        https://api.github.com/search/repositories?&q=created%3A%222017-03-11+..+2017-03-17%22+language%3AJava&type=Repositories

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(logging);

//        logging.setCacheControl(CacheControl.FORCE_NETWORK);


        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

    }
//
//    RequestInterceptor requestInterceptor = new RequestInterceptor() {
//        @Override
//        public void intercept(RequestFacade request) {
//            request.addHeader("Authorization", "amx "+sp.getPrefrenceStringdata(InstaremSharedPreferences.IE.AUTH_TOKEN, null));//"amx 0O1QCg+UcMLTHdfxHJllzWiUfWTw520EMifGt72vTDmRgMXZKJsx001K2Svelvuh");
//        }
//    };


    public static RestClient getInstance() {
        if (client == null) {
            client = new RestClient();
        }
        return client;
    }

    public APIService getApiService(Context context) {
        if (apiService == null) {
              apiService = retrofit.create(APIService.class);

            if(context != null) {
                sp = new InstaremSharedPreferences(context);
            }
        }
        return apiService;
    }

}
