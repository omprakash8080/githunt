package com.vsoft.hereyago.api;

import android.content.Context;

import com.vsoft.hereyago.api.common.CommonFacade;
import com.vsoft.hereyago.api.payee.PayeeFacade;
import com.vsoft.hereyago.api.repository.RepoFacade;
import com.vsoft.hereyago.api.user.UserFacade;

import java.util.HashMap;

public class IOCContainer {

	private static IOCContainer instance;
	public ResponsePublisher publisher;
	private Context context;

	private final static HashMap<ServiceName, Object> objectContainer = new HashMap<ServiceName, Object>();

	public enum ServiceName {
		AUTHENTICATE_SERVICE, PAYEE_SERVICE, USER_SERVICE, REPO_SERVICE, COMMON_SERVICE;
	}

	public Context getContext() {
		return context;
	}

	private IOCContainer() {
		publisher = new ResponsePublisher();
	}

	public static IOCContainer getInstance() {

		if (instance == null) {
			instance = new IOCContainer();
		}
		return instance;
	}

	public void init(Context context) {
		this.context = context;
	}

	public IBaseFacade getObject(ServiceName name, String tag) {

		IBaseFacade object = (IBaseFacade) objectContainer.get(name);

		if (object == null) {

			switch (name) {

				case AUTHENTICATE_SERVICE:
					object = new PayeeFacade(context, publisher);
					break;

				case PAYEE_SERVICE:
					object = new PayeeFacade(context, publisher);
					break;

				case USER_SERVICE:
					object = new UserFacade(context, publisher);
					break;

				case REPO_SERVICE:
					object = new RepoFacade(context, publisher);
					break;

				case COMMON_SERVICE:
					object = new CommonFacade(context, publisher);
					break;

			}
			objectContainer.put(name, object);
		}
		object.setTag(tag);

		return object;
	}
}
