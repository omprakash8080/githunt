package com.vsoft.hereyago.api.repository;

import android.content.Context;
import android.util.Log;

import com.vsoft.hereyago.Utils.TinyDB;
import com.vsoft.hereyago.api.APIService;
import com.vsoft.hereyago.api.IResponseSubscribe;
import com.vsoft.hereyago.api.RestClient;
import com.vsoft.hereyago.model.ReposResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by omprakash on 18/4/16.
 */
public class RepoFacade implements IRepoFacade {

    public static String TAG = "RepoFacade";
    private IResponseSubscribe responseObserver;
    APIService apiService;
    TinyDB tinyDB;

    public RepoFacade(Context context, IResponseSubscribe responseObserver) {
        this.responseObserver = responseObserver;
        apiService = RestClient.getInstance().getApiService(context);
        tinyDB = new TinyDB(context);
    }

    @Override
    public void onSuccess(Object response, String tag) {
        responseObserver.onSuccess(response, tag);
    }

    @Override
    public void onError(Throwable error, String tag) {
        responseObserver.onError(error, tag);
    }

    @Override
    public void getRepos(String startDate, String endDate, String lang, final String tag) {

        StringBuilder query = new StringBuilder();
//        query.append("Meituan-Dianping/Robust created:");
        query.append("created:");

        query.append(startDate);
        query.append("..");
        query.append(endDate);
//        query.append("&language:");
//        query.append(lang);

        Log.e(TAG, "lang : " + lang.toString());

        Call<ReposResponse> call = null;//apiService.getRepos(query.toString(),lang, lang);
        call.enqueue(new Callback<ReposResponse>() {
            @Override
            public void onResponse(Call<ReposResponse>call, Response<ReposResponse> response) {
//                List<Item> movies = response.body().getItems();
//                Log.d(TAG, "Number of movies received: " + movies.size());
                onSuccess(response.body(), tag);
            }

            @Override
            public void onFailure(Call<ReposResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                onError(t, tag);
            }
        });
    }

    @Override
    public void getRepos1( final String tag) {

        Call<ReposResponse> call = null;//apiService.getRepos1();
        call.enqueue(new Callback<ReposResponse>() {
            @Override
            public void onResponse(Call<ReposResponse>call, Response<ReposResponse> response) {
//                List<Item> movies = response.body().getItems();
//                Log.d(TAG, "Number of movies received: " + movies.size());
                onSuccess(response.body(), tag);
            }

            @Override
            public void onFailure(Call<ReposResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                onError(t, tag);
            }
        });
    }

    @Override
    public void getRepos2( final String tag) {

        Call<ReposResponse> call = null;//apiService.getRepos2();
        call.enqueue(new Callback<ReposResponse>() {
            @Override
            public void onResponse(Call<ReposResponse>call, Response<ReposResponse> response) {
//                List<Item> movies = response.body().getItems();
//                Log.d(TAG, "Number of movies received: " + movies.size());
                onSuccess(response.body(), tag);
            }

            @Override
            public void onFailure(Call<ReposResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                onError(t, tag);
            }
        });
    }


    @Override
    public void getReposAndroid(String _query, String lang, final String tag) {

        StringBuilder query = new StringBuilder();
        query.append(_query);
        query.append(" language:");
        query.append(lang);

        Log.e(TAG, "lang : " + query.toString());

//        q=android+language:java
        Call<ReposResponse> call = apiService.getReposNew(query.toString());
        call.enqueue(new Callback<ReposResponse>() {
            @Override
            public void onResponse(Call<ReposResponse>call, Response<ReposResponse> response) {
//                List<Item> movies = response.body().getItems();
//                Log.d(TAG, "Number of movies received: " + movies.size());
                onSuccess(response.body(), tag);
            }

            @Override
            public void onFailure(Call<ReposResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                onError(t, tag);
            }
        });
    }


    public void setTag(String tag) {

    }
}
