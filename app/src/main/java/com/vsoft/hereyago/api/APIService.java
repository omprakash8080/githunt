package com.vsoft.hereyago.api;


import com.vsoft.hereyago.model.ReposResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by omprakash on 30/03/16.
 */
public interface APIService {

//    search/repositories?&q=created%3A%222017-03-11+..+2017-03-17%22+language%3AJava&type=Repositories

//    @GET("search/repositories?utf8=%E2%9C%93&q=created%3A%222017-03-11+..+2017-03-17%22+language%3AHtml&type=Repositories&ref=advsearch&l=Html&l=\n")
//    Call<ReposResponse> getRepos1();
//
//    @GET("search/repositories?utf8=%E2%9C%93&q=created%3A%222017-03-11+..+2017-03-17%22+language%3AJava&type=Repositories&ref=advsearch&l=Java&l=\n")
//    Call<ReposResponse> getRepos2();
//
//
//    @GET("search/repositories?type=Repositories&ref=advsearch&l=")
//    Call<ReposResponse> getRepos(@Query("q") String strQuery, @Query("language") String lang, @Query("l") String l);


    @GET("search/repositories?sort=stars&order=desc&page=1")
    Call<ReposResponse> getReposNew(@Query("q") String strQuery);


//    https://api.github.com/search/repositories?q=android+language:java&sort=stars&order=desc&page=1

//    @GET("search/repositories?")
//    Call<ReposResponse> getReposAndroid(@Query("q") String strQuery, @Query("language") String lang);


}
