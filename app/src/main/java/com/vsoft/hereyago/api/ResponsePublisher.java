package com.vsoft.hereyago.api;

import java.util.concurrent.CopyOnWriteArrayList;

public class ResponsePublisher implements IResponseSubscribe {

	private CopyOnWriteArrayList<IResponseSubscribe> responseObservers;

	public ResponsePublisher() {
		responseObservers = new CopyOnWriteArrayList<IResponseSubscribe>();
	}

	public void registerResponseSubscribe(IResponseSubscribe responseObserver) {
		responseObservers.add(responseObserver);

	}

	public void unregisterResponseSubscribe(IResponseSubscribe responseObserver) {
		responseObservers.remove(responseObserver);
	}

	@Override
	public void onSuccess(Object response, String tag) {
		for (IResponseSubscribe observer : responseObservers) {
			observer.onSuccess(response, tag);
		}
	}

	@Override
	public void onError(Throwable error, String tag) {
		for (IResponseSubscribe observer : responseObservers) {
			observer.onError(error, tag);
		}
	}


}
