package com.vsoft.hereyago.Fragments;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.vsoft.hereyago.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_near_by)
public class NearByFragment extends BaseFragment {

    View rootView;
    String TAG = "HomeFragment";

//    @ViewById(R.id.toolbar)
//    Toolbar toolbar;

    @AfterViews
    void init() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Home");

    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }
}