package com.vsoft.hereyago.Fragments;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.AndroidUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;


@EFragment(R.layout.fragment_register_two)
public class RegisterFragmentTwo extends BaseFragment {

    View rootView;
    String TAG = "HomeFragment";
//    @ViewById(R.id.toolbar)
//    Toolbar toolbar;

    @ViewById(R.id.btn_next)
    Button btnNext;


    @AfterViews
    void init() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Sign Up");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);


    }

    @Click(R.id.btn_next)
    void loginClick(View view) {
        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.REGISTER_FRGMNT_PAGE_THREE), true,
                R.id.fragmentContainer, false);

    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }
}