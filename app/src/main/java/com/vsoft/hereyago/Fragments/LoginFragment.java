package com.vsoft.hereyago.Fragments;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.vsoft.hereyago.R;
import com.vsoft.hereyago.main.LandingActivity;
import com.vsoft.hereyago.main.MainActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_login)
public class LoginFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener{

    String TAG = "LoginFragment";

    @ViewById(R.id.input_email)
    EditText input_email;

    @ViewById(R.id.input_password)
    EditText input_password;

    @ViewById(R.id.sign_in_button)
    SignInButton signInButton;

    @ViewById(R.id.button_facebook_login)
    LoginButton buttonFacebookLogin;

    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private Context context;

    @AfterViews
    void init() {
        context = getActivity();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Login");

        signInButton.setSize(SignInButton.SIZE_STANDARD);

        mAuth = FirebaseAuth.getInstance();
        Log.d(TAG, "mAuth: " + mAuth);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]
//                updateUI(user);
                // [END_EXCLUDE]
            }
        };

        FacebookSdk.sdkInitialize(getActivity());
        AppEventsLogger.activateApp(getActivity());

    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }

    @Click(R.id.btn_login_instarem)
    void loginClick(View view) {
        signIn(input_email.getText().toString(), input_password.getText().toString());

    }

    public void signIn(String username, String password){
        Log.d(TAG, "signIn: " + username +" "+ password);

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(getActivity(), "Auth Failed",
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Logged In Successfully" , Toast.LENGTH_SHORT).show();


                            Intent in = new Intent(getActivity(), MainActivity_.class);
                            startActivity(in);

//                            mStatusTextView.setText(R.string.auth_failed);

                        }
                    }
                });
        // [END sign_in_with_email]

    }

    @Click(R.id.img_facebook)
    void facebookButtonClick(View view) {

    }

    @Click(R.id.img_google)
    void googleButtonClick(View view) {

    }

    @Click(R.id.img_twitter)
    void twitterButtonClick(View view) {

    }

    @Click(R.id.sign_in_button)
    void gmailButtonClick(View view) {
        gmailSignIn();
    }

    private void gmailSignIn() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        getActivity().startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Click(R.id.button_facebook_login)
    void fbLoginClick(View view) {
        LandingActivity landingActivity = (LandingActivity) context;
        landingActivity.fbLogin(buttonFacebookLogin);
    }

//    private void fbLogin() {
//
//        callbackManager = CallbackManager.Factory.create();
//
//        buttonFacebookLogin.setReadPermissions("email", "public_profile");
//        buttonFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.e(TAG, " ===========onSuccess================");
//                Log.e(TAG, "facebook:onSuccess:" + loginResult);
////                signInWithFacebook(loginResult.getAccessToken());
//            }
//
//            @Override
//            public void onCancel() {
//                Log.e(TAG, " ============onCancel===============");
//
//                Log.e(TAG, "facebook:onCancel");
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.e(TAG, " ===========onError================");
//
//                Log.e(TAG, "facebook:onError", error);
//            }
//        });
//    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e(TAG, "onConnectionFailed:" + connectionResult);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    //FaceBook
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, requestCode +" "+resultCode+ " "+ data );
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}