package com.vsoft.hereyago.Fragments;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.adapter.AllShareRecyclerAdapter;
import com.vsoft.hereyago.api.IOCContainer;
import com.vsoft.hereyago.api.repository.IRepoFacade;
import com.vsoft.hereyago.model.Item;
import com.vsoft.hereyago.model.ReposResponse;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_all_shares)
public class HtmlFragment extends BaseFragment {

    String TAG = "AllSharesFragment";
    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    private AllShareRecyclerAdapter adapter = null;
    List<Item> productList = new ArrayList<>();
    IRepoFacade repoFacade;
    boolean _areLecturesLoaded = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !_areLecturesLoaded ) {
            productList.clear();
            repoFacade = (IRepoFacade) IOCContainer.getInstance()
                    .getObject(IOCContainer.ServiceName.REPO_SERVICE, TAG);
            repoFacade.getReposAndroid("", "Html", TAG);

            _areLecturesLoaded = true;
        }
    }


    @AfterViews
    void init() {
        if(productList != null) {
            productList.clear();
        }

        adapter = new AllShareRecyclerAdapter(getActivity(), productList, new AllShareRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {

                try {

                    Log.e("item : ", "Clicked ");
//                    Intent in = new Intent(getActivity(), PayeeDetailActivity.class);
//                    in.putExtra("payeeTemporaryId", item.getRedirectId());
//                    getParentFragment().startActivityForResult(in, IsPayeeDeleted);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    public void onSelected(Context context, String lang){
//        if(lang != null) {
//
//        repoFacade = (IRepoFacade) IOCContainer.getInstance()
//                        .getObject(IOCContainer.ServiceName.REPO_SERVICE, TAG);
//        repoFacade.getRepos("2017-03-11", "2017-03-17", lang, TAG);
//
//        Toast.makeText(context,
//                    "Loaded Succesfully "+lang, Toast.LENGTH_SHORT).show();
//        }

    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        if(response instanceof ReposResponse){
            ReposResponse reposResponse = (ReposResponse) response;
            productList.clear();
            productList.addAll(reposResponse.getItems());
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onError(Throwable error, String tag) {

    }

}