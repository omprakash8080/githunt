package com.vsoft.hereyago.Fragments;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.AndroidUtils;
import com.vsoft.hereyago.adapter.OnBoardingTabAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Omprakash on 8-July-2016.
 */

@EFragment(R.layout.fragment_on_boarding_pager)
public class OnBoardingMainFragment extends BaseFragment {

    static String TAG = "HomeFragment";

    @ViewById(R.id.pager)
    ViewPager mViewPager;

    @ViewById(R.id.tabDots)
    TabLayout tabLayout;

//    @ViewById(R.id.toolbar)
//    Toolbar toolbar;
    OnBoardingTabAdapter fragAdapter;


    @AfterViews
    void init() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        toolbar.setTitle("Home");

//        txtMessage.setText(Html.fromHtml("<b> Welcome To The Sharing Universe </b>" +  "<br />" +
//                "<small> Share what you want with who you want! </small>" + "<br />"
//        ));

        fragAdapter = new OnBoardingTabAdapter(getChildFragmentManager(), getActivity());
        mViewPager.setAdapter(fragAdapter);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Click(R.id.btn_login)
    void loginClick(View view) {

        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.LOGIN_FRAGMENT), true,
                R.id.fragmentContainer, false);

    }


    @Click(R.id.btn_signup)
    void signUpClick(View view) {

        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.REGISTER_FRGMNT_PAGE_ONE), true,
                R.id.fragmentContainer, false);

    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }
}