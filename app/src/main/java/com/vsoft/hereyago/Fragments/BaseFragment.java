package com.vsoft.hereyago.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.Constants;
import com.vsoft.hereyago.api.IOCContainer;
import com.vsoft.hereyago.api.IResponseSubscribe;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public abstract class BaseFragment extends Fragment implements IResponseSubscribe, Constants {

    protected Activity activityContext;
    ProgressBar progressBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityContext = (Activity)context;
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressbar);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, boolean animate) {

        if (getActivity() != null) {
            if (!getActivity().isFinishing()) {
                FragmentTransaction fragmentTransaction = getActivity()
                        .getSupportFragmentManager().beginTransaction();
                if (animate) {
//					ApplyAnimation.toFragment(fragmentTransaction,
//							AnimationEffect.TRANSALTE_IN,
//							AnimationEffect.TRANSALTE_OUT);
                }

                fragmentTransaction.replace(layout_id, fragment,
                        fragment.getTag());
                if (addtoBackStack) {
                    fragmentTransaction.addToBackStack(fragment.getTag());
                }

                fragmentTransaction.commit();
            }
        }
    }

    public static void toggleVisibility(boolean flag, ProgressBar progressBar) {
        if(progressBar != null){
            progressBar.setVisibility(flag ? View.VISIBLE: View.GONE);
        }
    }

    void progressBarVisibility(boolean isVisible) {

        if (progressBar == null) {
            return;
        }

        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.
                    setVisibility(View.INVISIBLE);
        }

    }

    void setSpinnerAdapter(MaterialBetterSpinner spinner, String[] dataArray) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_dropdown_item, dataArray);
        spinner.setAdapter(arrayAdapter);

    }

    public static Fragment getFragmentByTag(String tabTag){

        Fragment fragment = null;

        if(tabTag.equalsIgnoreCase(ARDUINO)){
            fragment = new ArduinoFragment_();

        }else if(tabTag.equalsIgnoreCase(ANDROID)){
            fragment = new AndroidFragment_();

        }else if(tabTag.equalsIgnoreCase(JAVA)){
            fragment = new JavaFragment_();

        }else if(tabTag.equalsIgnoreCase(HTML)){
            fragment = new HtmlFragment_();

        }else if(tabTag.equalsIgnoreCase(JAVASCRIPT)){
            fragment = new JavaScriptFragment_();

        }else if(tabTag.equalsIgnoreCase(DART)){
            fragment = new DartFragment_();

        }else if(tabTag.equalsIgnoreCase(CLOJURE)){
            fragment = new ClojureFragment_();

        }else if(tabTag.equalsIgnoreCase(CSHARP)){
            fragment = new CsharpFragment_();

        }else if(tabTag.equalsIgnoreCase(CPLUSPLUS)){
            fragment = new CPlusPlusFragment_();

        }else if(tabTag.equalsIgnoreCase(C)){
            fragment = new CFragment_();

        }else if(tabTag.equalsIgnoreCase(OBJECTIVEC)){
            fragment = new ObjectiveCFragment_();

        }else if(tabTag.equalsIgnoreCase(CSS)){
            fragment = new CssFragment_();

        }else if(tabTag.equalsIgnoreCase(GO)){
            fragment = new GoFragment_();

        }else if(tabTag.equalsIgnoreCase(PYTHON)){
            fragment = new PythonFragment_();

        }else if(tabTag.equalsIgnoreCase(RUBY)){
            fragment = new RubyFragment_();

        }else if(tabTag.equalsIgnoreCase(SCALA)){
            fragment = new ScalaFragment_();

        }else if(tabTag.equalsIgnoreCase(GROOVY)){
            fragment = new GroovyFragment_();

        }else if(tabTag.equalsIgnoreCase(SCHEME)){
            fragment = new SchemeFragment_();

        }else if(tabTag.equalsIgnoreCase(JFLEX)){
            fragment = new JFlexFragment_();

        }else if(tabTag.equalsIgnoreCase(PHP)){
            fragment = new PhpFragment_();

        }else if(tabTag.equalsIgnoreCase(SWIFT)){
            fragment = new SwiftFragment_();

        }else {
            fragment = new TrendingFragment_();
        }

        return fragment;
    }

}
