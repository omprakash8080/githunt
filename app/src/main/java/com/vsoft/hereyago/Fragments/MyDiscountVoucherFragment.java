package com.vsoft.hereyago.Fragments;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.AndroidUtils;
import com.vsoft.hereyago.adapter.AllShareRecyclerAdapter;
import com.vsoft.hereyago.model.Item;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_my_discount_voucher)
public class MyDiscountVoucherFragment extends BaseFragment {

    String TAG = "HomeFragment";
    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    private AllShareRecyclerAdapter adapter = null;
    List<Item> productList = new ArrayList<>();

    @AfterViews
    void init() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("My Gift Cards");

        if(productList != null) {
            productList.clear();
        }

        FirebaseDatabase firebaseDatabase = AndroidUtils.getFirebaseDatabaseInsatance();
        DatabaseReference mDatabase = firebaseDatabase.getReference("products");

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.e("Count " ,""+snapshot.getChildrenCount());
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Item product = postSnapshot.getValue(Item.class);

                    productList.add(product);
                    adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
            }

        });

        adapter = new AllShareRecyclerAdapter(getActivity(), productList, new AllShareRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {

                try {

                    Log.e("item : ", "Clicked ");
//                    Intent in = new Intent(getActivity(), PayeeDetailActivity.class);
//                    in.putExtra("payeeTemporaryId", item.getRedirectId());
//                    getParentFragment().startActivityForResult(in, IsPayeeDeleted);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }

}