package com.vsoft.hereyago.Fragments;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.adapter.CommunityTabAdapter;
import com.vsoft.hereyago.common.SlidingTabLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Omprakash on 8-July-2016.
 */

@EFragment(R.layout.fragment_community)
public class CommunityFragment extends BaseFragment {

    String TAG = "HomeFragment";

    @ViewById(R.id.pager)
    ViewPager mpager;
    @ViewById(R.id.tabs)
    SlidingTabLayout mtabs;

    CommunityTabAdapter fragAdapter;


    @AfterViews
    void init() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Community");

        fragAdapter = new CommunityTabAdapter(getChildFragmentManager(), getActivity());
        mpager.setAdapter(fragAdapter);

        mtabs.setDistributeEvenly(true);
        mtabs.setViewPager(mpager);
        mtabs.setActivated(false);
        mtabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });
    }

//
//    @Click(R.id.btn_signup)
//    void signUpClick(View view) {
//
//        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.REGISTER_FRGMNT_PAGE_ONE), true,
//                R.id.fragmentContainer, false);
//    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }

}