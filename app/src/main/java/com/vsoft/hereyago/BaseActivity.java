package com.vsoft.hereyago;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ProgressBar;

import com.vsoft.hereyago.Fragments.BaseFragment;
import com.vsoft.hereyago.Utils.AndroidUtils;
import com.vsoft.hereyago.Utils.CallbackInterface;
import com.vsoft.hereyago.Utils.Constants;
import com.vsoft.hereyago.api.IOCContainer;
import com.vsoft.hereyago.api.IResponseSubscribe;

public abstract class BaseActivity extends AppCompatActivity implements
        CallbackInterface, IResponseSubscribe, Constants {

    protected ActionBar actionBar;
    //    protected String[] navOptions;
    public static String lastErrorMsg = null;
    static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    protected void ignoreTouchMotion(View view) {
        view.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }



  @Override
    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, boolean animate) {

        setTitle(getString(R.string.app_name));
        if (!isFinishing()) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            if (animate) {
                //fragmentTransaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);
            }
            fragmentTransaction.replace(layout_id, fragment);
            if (addtoBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        }

    }


   @Override
    public void switchtoFragment(int tag, boolean addtoBackStack,
                                 int layout_id, boolean animate) {
        BaseFragment fragment = AndroidUtils.getFragment(tag);

        if (fragment != null) {
//            fragment.setListener(this);
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            if (animate) {
//                transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out, R.anim.slide_right_in, R.anim.slide_right_out);
            }
            transaction.replace(layout_id, fragment);

            if (addtoBackStack) {
                transaction.addToBackStack(null);
            }
            //Kindly checkout for following bug statement.
            //cannot perform this action after onsaveinstancestate  checkstateloss on  setResult(Activity.RESULT_OK)
            transaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void switchtoFragment(int tag, boolean addtoBackStack,
                                 int layout_id, boolean animate, Bundle bundle) {
        BaseFragment fragment = AndroidUtils.getFragment(tag);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        switchFragment(fragment, addtoBackStack, layout_id, animate);

    }

    public static void toggleVisibility(boolean flag, ProgressBar progressBar) {
        if(progressBar != null){
            progressBar.setVisibility(flag ? View.VISIBLE : View.GONE);
        }
    }

}
