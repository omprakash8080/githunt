package com.vsoft.hereyago.adapter;

/**
 * Created by om on 1/9/17.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.vsoft.hereyago.Fragments.BaseFragment;
import com.vsoft.hereyago.Utils.TinyDB;

import java.util.ArrayList;

public class HomeTabAdapter extends SmartFragmentStatePagerAdapter {

//    public static String tabs[] = {"Android", "Java", "Html"};
    ArrayList<String> tabs;
    TinyDB tinyDB;
    Context ctx;


    public HomeTabAdapter(FragmentManager fm, Context _ctx) {
        super(fm);
        ctx = _ctx;
        tinyDB = new TinyDB(_ctx);
        tabs = tinyDB.getListString("tabs");
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
//        if(i == 0) {
            fragment = BaseFragment.getFragmentByTag(tabs.get(i));
//             fragment = new AllSharesFragment_();
//         }

//        if(i == 1) {
//            fragment = new AndroidFragment_();
//        }
//
//        if(i == 2) {
//            fragment = new JavaFragment_();
//        }
//
//        if(i == 3) {
//            fragment = new JavaScriptFragment_();
//        }
//
//        if(i == 4) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 5) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 6) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 7) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 8) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 9) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 10) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 11) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 12) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 13) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 14) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 15) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 16) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 17) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 18) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 19) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 20) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 21) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 22) {
//            fragment = new AllSharesFragment_();
//        }
//
//        if(i == 23) {
//            fragment = new AllSharesFragment_();
//        }

        return fragment;
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return  tabs.get(position);
    }


}