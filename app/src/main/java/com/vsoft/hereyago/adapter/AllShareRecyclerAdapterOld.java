package com.vsoft.hereyago.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.model.Product;

import java.util.List;

/**
 * Created by om on 1/17/17.
 */

public class AllShareRecyclerAdapterOld extends RecyclerView.Adapter
        <AllShareRecyclerAdapterOld.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Product item);
    }

    private final List<Product> items;
    private final OnItemClickListener listener;
    static Context ctx;

    public AllShareRecyclerAdapterOld(Context ctx, List<Product> items, OnItemClickListener listener) {
        this.ctx = ctx;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.landing_item_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
//        ImageView productImageView;
        TextView postedTime;
        TextView accnumber;
//        RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.txt_product_title);
//            productImageView = (ImageView) itemView.findViewById(R.id.img_product);
            postedTime = (TextView) itemView.findViewById(R.id.txt_posted_time);

//            bankName = (TextView)itemView.findViewById(R.id.txt_bankName);
//            accnumber = (TextView)itemView.findViewById(R.id.txt_bankAccount);
//            layout = (RelativeLayout)itemView.findViewById(R.id.lay_header);
        }

        public void bind(final Product item, final OnItemClickListener listener) {

            title.setText(item.getTitle());

            Log.e("adapter ", item.getPostedOn() + " 8080");
            postedTime.setText(item.getPostedOn());
//            Glide.with(ctx)
//                    .load(item.getImage())
////                    .centerCrop()
//                    .placeholder(R.drawable.ic_all_login_bg)
//                    .crossFade()
//                    .into(productImageView);

//            bankName.setText(item.getBank());
//            accnumber.setText(item.getAccountNumber());
//
//            if(item.getAccountNumber().length()>8){
//                accnumber.setText(item.getAccountNumber().substring(item.getAccountNumber().length() - 6));
//            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}

