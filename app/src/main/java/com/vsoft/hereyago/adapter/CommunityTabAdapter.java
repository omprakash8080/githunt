package com.vsoft.hereyago.adapter;

/**
 * Created by om on 1/9/17.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vsoft.hereyago.Fragments.AllSharesFragment_;
import com.vsoft.hereyago.Fragments.MembersFragment_;
import com.vsoft.hereyago.Fragments.MessagesFragment;
import com.vsoft.hereyago.Fragments.MessagesFragment_;
import com.vsoft.hereyago.Fragments.NearByFragment_;

public class CommunityTabAdapter extends FragmentPagerAdapter {

    String tabs[] = {"Members", "Messages"};
    Context ctx;

    public CommunityTabAdapter(FragmentManager fm, Context _ctx) {
        super(fm);
        ctx = _ctx;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if(i == 0) {
            fragment = new MembersFragment_();
        }

        if(i == 1) {
            fragment = new MessagesFragment_();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return  tabs[position];
    }


}