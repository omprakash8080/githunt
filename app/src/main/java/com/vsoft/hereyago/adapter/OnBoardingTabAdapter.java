package com.vsoft.hereyago.adapter;

/**
 * Created by om on 1/9/17.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vsoft.hereyago.Fragments.OnBoardingFragmentOne_;
import com.vsoft.hereyago.Fragments.OnBoardingFragmentThree_;
import com.vsoft.hereyago.Fragments.OnBoardingFragmentTwo_;

public class OnBoardingTabAdapter extends FragmentPagerAdapter {

    String tabs[] = {"", "", ""};
    Context ctx;

    public OnBoardingTabAdapter(FragmentManager fm, Context _ctx) {
        super(fm);
        ctx = _ctx;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if(i == 0) {
            fragment = new OnBoardingFragmentOne_();
        }

        if(i == 1) {
            fragment = new OnBoardingFragmentTwo_();
        }

        if(i == 2) {
            fragment = new OnBoardingFragmentThree_();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return  tabs[position];
    }


}