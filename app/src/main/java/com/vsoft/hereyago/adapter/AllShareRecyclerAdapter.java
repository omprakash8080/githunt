package com.vsoft.hereyago.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.model.Item;

import java.util.List;

/**
 * Created by om on 1/17/17.
 */

public class AllShareRecyclerAdapter extends RecyclerView.Adapter
        <AllShareRecyclerAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Item item);
    }

    private final List<Item> items;
    private final OnItemClickListener listener;
    static Context ctx;

    public AllShareRecyclerAdapter(Context ctx, List<Item> items, OnItemClickListener listener) {
        this.ctx = ctx;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.githunt_item_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView desc;
        TextView name;
        Button forks;
        Button watchers;
        Button openIssue;

        TextView postedTime;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.txt_name);
            desc = (TextView) itemView.findViewById(R.id.txt_desc);
            forks = (Button) itemView.findViewById(R.id.btn_forks);
            watchers = (Button) itemView.findViewById(R.id.btn_watchers);
            openIssue = (Button) itemView.findViewById(R.id.btn_open_issues);

//            postedTime = (TextView) itemView.findViewById(R.id.txt_posted_time);
        }

        public void bind(final Item item, final OnItemClickListener listener) {

            name.setText(item.getFullName());
            desc.setText(item.getDescription());
            forks.setText(String.valueOf(item.getForks()));
            watchers.setText(String.valueOf(item.getWatchers()));
            openIssue.setText(String.valueOf(item.getOpenIssues()));

//            postedTime.setText(item.getCreatedAt());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}

