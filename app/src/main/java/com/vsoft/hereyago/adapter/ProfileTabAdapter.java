package com.vsoft.hereyago.adapter;

/**
 * Created by om on 1/9/17.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vsoft.hereyago.Fragments.AllSharesFragment_;
import com.vsoft.hereyago.Fragments.NearByFragment_;
import com.vsoft.hereyago.Fragments.TrendingFragment_;

public class ProfileTabAdapter extends FragmentPagerAdapter {

    String tabs[] = {"All Shares", "My Badges", "My Shines"};
    Context ctx;

    public ProfileTabAdapter(FragmentManager fm, Context _ctx) {
        super(fm);
        ctx = _ctx;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if(i == 0) {
            fragment = new AllSharesFragment_();
        }

        if(i == 1) {
            fragment = new NearByFragment_();
        }

        if(i == 2) {
            fragment = new TrendingFragment_();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return  tabs[position];
    }


}