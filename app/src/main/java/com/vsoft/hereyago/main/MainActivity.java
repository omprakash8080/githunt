package com.vsoft.hereyago.main;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.vsoft.hereyago.BaseActivity;
import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.AndroidUtils;
import com.vsoft.hereyago.Utils.TinyDB;
import com.vsoft.hereyago.adapter.HomeTabAdapter;
import com.vsoft.hereyago.api.user.IUserFacade;
import com.vsoft.hereyago.common.InstaremSharedPreferences;
import com.vsoft.hereyago.common.SlidingTabLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener  {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.fragmentContainer)
    RelativeLayout fragmentContainer;
    @ViewById(R.id.pager)
    ViewPager mpager;
    @ViewById(R.id.tabs)
    SlidingTabLayout mtabs;

    TinyDB tinyDB;
    IUserFacade userFacade;
    String TAG = "MainActivity";
    HomeTabAdapter fragAdapter;
    private InstaremSharedPreferences sp = null;

    @AfterViews
    void init() {
        tinyDB = new TinyDB(getApplicationContext());

        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("GitHunt");
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_timeline);
        navigationView.setNavigationItemSelectedListener(this);

        fragAdapter = new HomeTabAdapter(getSupportFragmentManager(), getApplicationContext());
        mpager.setCurrentItem(0);
        mpager.setAdapter(fragAdapter);

//        mtabs.setCustomTabView(R.layout.custom_sliding_view, R.id.tabImg);
        mtabs.setDistributeEvenly(true);
        mtabs.setViewPager(mpager);
        mtabs.setActivated(false);
        mtabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        mpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {

//                Log.e(TAG, "position "+ position);
//                AllSharesFragment fragment = (AllSharesFragment) fragAdapter.getItem(position);
//                fragment.onSelected(MainActivity.this, fragAdapter.tabs[position]);//do stuff in here
//
//                Log.e(TAG, "position "+ position);
//                Log.e(TAG, "AllSharesFragment "+ fragment);
//                Log.e(TAG, "lang "+ fragAdapter.tabs[position]);
//                Toast.makeText(MainActivity.this,
//                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });



    }

    void purgeData() {
        tinyDB.putObject("ProfileInfo", null);
        tinyDB.putObject("AddPayeeModel", null);
        tinyDB.putObject("LoginResponse", null);
        tinyDB.putObject("VerifyAccessResponse", null);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(R.id.action_setting == id){

            ArrayList<String> tabs = tinyDB.getListString("tabs");
//            if(tabs.contains("Html"))
//            tabs.add("Html");
//            tinyDB.putListString("tabs",tabs);

//            finish();
            startActivityForResult(new Intent(MainActivity.this, SettingActivity_.class), 2);
//            startActivity();
//            ProcessPhoenix.triggerRebirth(this);
//            startActivity(new Intent(MainActivity.this, LandingActivity_.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 2){

            finish();
            startActivity(new Intent(MainActivity.this, MainActivity_.class));

        }

//        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//            if (fragment != null) {
//                fragment.onActivityResult(requestCode, resultCode, data);
//            }
//        }
    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        mtabs.setVisibility(View.GONE);
        mpager.setVisibility(View.GONE);
        fragmentContainer.setVisibility(View.GONE);


        if (id == R.id.nav_timeline) {
            mtabs.setVisibility(View.VISIBLE);
            mpager.setVisibility(View.VISIBLE);

        } else if (id == R.id.nav_my_profile) {
            fragmentContainer.setVisibility(View.VISIBLE);
            switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.MY_PROFILE),
                    false, R.id.fragmentContainer, false);

        } else if (id == R.id.nav_the_big_picture) {

        } else if (id == R.id.nav_community) {

            fragmentContainer.setVisibility(View.VISIBLE);
            switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.COMMUNITY_FRAGMENT),
                    false, R.id.fragmentContainer, false);

        } else if (id == R.id.nav_leaderboard) {
            fragmentContainer.setVisibility(View.VISIBLE);
            switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.LEADERBOARD_FRAGMENT),
                    false, R.id.fragmentContainer, false);


        } else if (id == R.id.nav_my_voucher_and_gift) {

            fragmentContainer.setVisibility(View.VISIBLE);
            switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.MY_DISCOUNT_VOUCHER_FRAGMENT),
                    false, R.id.fragmentContainer, false);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
