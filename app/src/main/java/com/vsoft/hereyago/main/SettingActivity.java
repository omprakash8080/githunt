package com.vsoft.hereyago.main;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.GridLayout;

import com.vsoft.hereyago.BaseActivity;
import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.TinyDB;
import com.vsoft.hereyago.adapter.HomeTabAdapter;
import com.vsoft.hereyago.api.user.IUserFacade;
import com.vsoft.hereyago.common.InstaremSharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_setting)
public class SettingActivity extends BaseActivity {

    TinyDB tinyDB;
    IUserFacade userFacade;
    String TAG = "SettingActivity";
    HomeTabAdapter fragAdapter;
    private InstaremSharedPreferences sp = null;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.cb_arduino)
    CheckBox cb_arduino;
    @ViewById(R.id.cb_android)
    CheckBox cb_android;
    @ViewById(R.id.cb_c)
    CheckBox cb_c;
    @ViewById(R.id.cb_cplusplus)
    CheckBox cb_cplusplus;
    @ViewById(R.id.cb_css)
    CheckBox cb_css;
    @ViewById(R.id.cb_groovy)
    CheckBox cb_groovy;
    @ViewById(R.id.cb_html)
    CheckBox cb_html;
    @ViewById(R.id.cb_java)
    CheckBox cb_java;
    @ViewById(R.id.cb_javascript)
    CheckBox cb_javascript;
    @ViewById(R.id.cb_jflex)
    CheckBox cb_jflex;
    @ViewById(R.id.cb_php)
    CheckBox cb_php;
    @ViewById(R.id.cb_dart)
    CheckBox cb_dart;
    @ViewById(R.id.cb_clojure)
    CheckBox cb_clojure;
    @ViewById(R.id.cb_csharp)
    CheckBox cb_csharp;
    @ViewById(R.id.cb_objective_c)
    CheckBox cb_objective_c;
    @ViewById(R.id.cb_go)
    CheckBox cb_go;
    @ViewById(R.id.cb_python)
    CheckBox cb_python;
    @ViewById(R.id.cb_ruby)
    CheckBox cb_ruby;
    @ViewById(R.id.cb_scala)
    CheckBox cb_scala;
    @ViewById(R.id.cb_scheme)
    CheckBox cb_scheme;
    @ViewById(R.id.cb_sql)
    CheckBox cb_sql;
    @ViewById(R.id.cb_swift)
    CheckBox cb_swift;

    @AfterViews
    void init() {
        tinyDB = new TinyDB(getApplicationContext());

        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Setting");
        setSupportActionBar(toolbar);

        setUpCheckboxToggleState();

    }

    void setUpCheckboxToggleState(){

        GridLayout root = (GridLayout) findViewById(R.id.grid_layout); //or whatever your root control is

        for(int i = 0; i < root.getChildCount(); i++) {
            View child = root.getChildAt(i);
            if (child instanceof CheckBox) {
                CheckBox checkBox = (CheckBox) child;
                toggleCheckbox(checkBox);
            }
        }
    }

    void toggleCheckbox(CheckBox checkBox){
        String title = checkBox.getText().toString();
        ArrayList<String> tabs = tinyDB.getListString("tabs");
        if(tabs.contains(title)) {
            checkBox.setChecked(true);

        }else {
            checkBox.setChecked(false);
        }

    }

    @Override
    public void onBackPressed() {
        Intent intentMessage=new Intent();
        setResult(2,intentMessage);
        super.onBackPressed();

//        moveTaskToBack(true);
    }

    public void onCheckboxClicked(final View view) {
        switch (view.getId()) {
            case R.id.cb_arduino:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_android:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_c:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_csharp:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_css:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_groovy:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_html:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_java:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_javascript:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_jflex:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_php:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_dart:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_clojure:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_cplusplus:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_objective_c:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;
            case R.id.cb_go:
                Log.e(TAG, view+"");
                toggleCheckbox(view);

                break;

            case R.id.cb_python:
                toggleCheckbox(view);
                break;

            case R.id.cb_ruby:
                toggleCheckbox(view);
                break;

            case R.id.cb_scala:
                toggleCheckbox(view);
                 break;

            case R.id.cb_scheme:
                toggleCheckbox(view);
                break;

            case R.id.cb_sql:
                toggleCheckbox(view);
                break;

            case R.id.cb_swift:
                toggleCheckbox(view);
                break;

        }
    }

    void toggleCheckbox(View view){
        boolean checked = ((CheckBox) view).isChecked();
        String title = ((CheckBox) view).getText().toString();
        if (checked) {
            ArrayList<String> tabs = tinyDB.getListString("tabs");
            if(!tabs.contains(title)) {
                tabs.add(title);
                tinyDB.putListString("tabs", tabs);
            }

        }
        else {
            ArrayList<String> tabs = tinyDB.getListString("tabs");
            tabs.remove(title);
            tinyDB.putListString("tabs",tabs);
        }
    }

    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onError(Throwable error, String tag) {

    }

}
