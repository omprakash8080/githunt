package com.vsoft.hereyago.main;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vsoft.hereyago.BaseActivity;
import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.AndroidUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@EActivity(R.layout.activity_landing)
public class LandingActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {
    public static String TAG = "LandingActivity";

    private CallbackManager callbackManager;


    @AfterViews
    void init() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.vsoft.hereyago",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.ON_BOARDING_FRAGMENT),
                false, R.id.fragmentContainer, false);
//        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.ON_BOARDING_FRAGMENT), false,
//                R.id.fragmentContainer, false);
    }


    @Override
    public void onSuccess(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }
    }

    @Override
    public void onError(Throwable error, String tag) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e(TAG, "username: " + acct.getDisplayName());
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
//            updateUI(true);
        } else {
            Log.e(TAG, "signout: " + " done");
//
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    public void fbLogin(LoginButton buttonFacebookLogin) {

        callbackManager = CallbackManager.Factory.create();

        buttonFacebookLogin.setReadPermissions("email", "public_profile");
        buttonFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, " ===========onSuccess================");
                Log.e(TAG, "facebook:onSuccess:" + loginResult);
//                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, " ============onCancel===============");

                Log.e(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, " ===========onError================");

                Log.e(TAG, "facebook:onError", error);
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
