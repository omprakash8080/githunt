package com.vsoft.hereyago.Utils;

/**
 * Created by omprakash on 10/6/16.
 */
public interface Constants {

    public static final int RC_SIGN_IN = 111;
    public static String ARDUINO = "Arduino";
    public static String DART = "Dart";
    public static String ANDROID = "Android";
    public static String CLOJURE = "Clojure";
    public static String CSHARP = "CSharp";
    public static String CPLUSPLUS = "C++";
    public static String C = "C";
    public static String OBJECTIVEC = "Objective-C";
    public static String CSS = "Css";
    public static String GO = "Go";
    public static String GROOVY = "Groovy";
    public static String PYTHON = "Python";
    public static String HTML = "HTML";
    public static String RUBY = "Ruby";
    public static String JAVA = "Java";
    public static String SCALA = "Scala";
    public static String JAVASCRIPT = "JavaScript";
    public static String SCHEME = "Scheme";
    public static String JFLEX = "JFlex";
    public static String PHP = "PHP";
    public static String SWIFT = "Swift";

}
