package com.vsoft.hereyago.Utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Base64;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;
import com.vsoft.hereyago.Fragments.BaseFragment;
import com.vsoft.hereyago.Fragments.CommunityFragment;
import com.vsoft.hereyago.Fragments.CommunityFragment_;
import com.vsoft.hereyago.Fragments.HomeFragment_;
import com.vsoft.hereyago.Fragments.LeaderboardFragment;
import com.vsoft.hereyago.Fragments.LeaderboardFragment_;
import com.vsoft.hereyago.Fragments.LoginFragment_;
import com.vsoft.hereyago.Fragments.MyDiscountVoucherFragment;
import com.vsoft.hereyago.Fragments.MyDiscountVoucherFragment_;
import com.vsoft.hereyago.Fragments.MyProfileFragment;
import com.vsoft.hereyago.Fragments.MyProfileFragment_;
import com.vsoft.hereyago.Fragments.OnBoardingMainFragment_;
import com.vsoft.hereyago.Fragments.RegisterFragmentThree;
import com.vsoft.hereyago.Fragments.RegisterFragmentThree_;
import com.vsoft.hereyago.Fragments.RegisterFragmentTwo_;
import com.vsoft.hereyago.Fragments.RegisterFragment_;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidUtils {

    private static FirebaseDatabase mDatabase;
    private static boolean isTablet;
    public static int ORIENTATION_FLAG;
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";

    public static final String[] chooseOptions = new String[]{"Take from camera",
            "Select from gallery", "Select PDF from sd card"};


    public static String[] permissionsMarshmellow = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.STORAGE", "android.permission.CAMERA"};

    public static void init(String screenType) {
        if (screenType.equalsIgnoreCase("tablet")) {
            isTablet = true;
        }
        setOrientation();
    }

    private static void setOrientation() {

        if (isTablet) {
            ORIENTATION_FLAG = ActivityInfo.SCREEN_ORIENTATION_USER;
        } else {
            ORIENTATION_FLAG = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        }
    }

    public static boolean isTablet() {
        return isTablet;
    }

    public static boolean isJellyBeanOrHigher() {

        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean isLollipopOrHigher(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    //Unused method

    /*public static void makeLinkClickable(SpannableStringBuilder strBuilder,
                                         final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        // URLSpan[] spans = strBuilder.getSpans(0, strBuilder.length(),
        // URLSpan.class);

        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                HtmlLinkClickEvent event = new HtmlLinkClickEvent();
                event.url = span.getURL();
                BusProvider.getInstance().post(event);

            }
        };

        strBuilder.setSpan(clickable, start, end, flags);
        URLSpan span1 = new URLSpanNoUnderline(span.getURL());
        strBuilder.setSpan(span1, start, end, flags);
        strBuilder.removeSpan(span);

    }*/

   /* public static void setHTMLText(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(),
                URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
    }*/

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(final String password) {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();

    }

    public static Integer getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageInt;
    }


    public static String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public static byte[] readFile(File file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }

    public static Intent getImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        return intent;
    }

    public static Intent getPdfIntent() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        return intent;
    }

    public static void getDate(Activity act, final TextView textview, boolean isAllowToSelectOldDates) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        if (textview.getText().toString().length() != 0) {

            // set the previous date if it was selected
            String previousDate[] = textview.getText().toString()
                    .split("/");
            day = Integer.parseInt(previousDate[0]);
            month = Integer.parseInt(previousDate[1])-1;
            year = Integer.parseInt(previousDate[2]);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(act,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view,
                                          final int year, final int monthOfYear,
                                          final int dayOfMonth) {

                        textview.setText(dayOfMonth + "/"
                                + (monthOfYear + 1) + "/" + year);

                    }
                }, year, month, day);

        if(!isAllowToSelectOldDates){
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }
        datePickerDialog.show();
    }

    public static FirebaseDatabase getFirebaseDatabaseInsatance() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(false);
        }
        return mDatabase;
    }

    public static class FragmentTag {
        public static final int
                HOME_FRAGMENT=0, LOGIN_FRAGMENT=1, REGISTER_FRGMNT_PAGE_ONE =2, ON_BOARDING_FRAGMENT=3,
                REGISTER_FRGMNT_PAGE_TWO =4, REGISTER_FRGMNT_PAGE_THREE =5, MY_PROFILE=6,
                COMMUNITY_FRAGMENT=7, LEADERBOARD_FRAGMENT=8, MY_DISCOUNT_VOUCHER_FRAGMENT=9;


    }

    public static BaseFragment getFragment(int fragmentTag) {
        BaseFragment fragment = null;

        switch (fragmentTag) {

            case FragmentTag.ON_BOARDING_FRAGMENT:
                fragment = new OnBoardingMainFragment_();
                break;

            case FragmentTag.HOME_FRAGMENT:
                fragment = new HomeFragment_();
                break;

            case FragmentTag.LOGIN_FRAGMENT:
                fragment = new LoginFragment_();
                break;

            case FragmentTag.REGISTER_FRGMNT_PAGE_ONE:
                fragment = new RegisterFragment_();
                break;

            case FragmentTag.REGISTER_FRGMNT_PAGE_TWO:
                fragment = new RegisterFragmentTwo_();
                break;

            case FragmentTag.REGISTER_FRGMNT_PAGE_THREE:
                fragment = new RegisterFragmentThree_();
                break;

            case FragmentTag.MY_PROFILE:
                fragment = new MyProfileFragment_();
                break;

            case FragmentTag.COMMUNITY_FRAGMENT:
                fragment = new CommunityFragment_();
                break;

            case FragmentTag.LEADERBOARD_FRAGMENT:
                fragment = new LeaderboardFragment_();
                break;

            case FragmentTag.MY_DISCOUNT_VOUCHER_FRAGMENT:
                fragment = new MyDiscountVoucherFragment_();
                break;

            default:
                break;

        }
        return fragment;
    }

}
