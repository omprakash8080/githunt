package com.vsoft.hereyago.common;

import android.app.Activity;
import android.graphics.Color;

import com.github.johnpersano.supertoasts.SuperActivityToast;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

/**
 * Created by omprakash on 5/4/16.
 */
public class AppUtil {

    public static void showSuperToast(Activity activity, boolean isSuceess, String message) {
      final Style customStyle = new Style();
        customStyle.animations = SuperToast.Animations.FLYIN;
      if(!isSuceess){
        customStyle.background = SuperToast.Background.RED;
      }else {
        customStyle.background = SuperToast.Background.GRAY;
      }
        customStyle.textColor = Color.WHITE;
        customStyle.buttonTextColor = Color.LTGRAY;
        customStyle.dividerColor = Color.WHITE;

        final SuperActivityToast superActivityToast = new SuperActivityToast(activity, customStyle);
        superActivityToast.setDuration(SuperToast.Duration.SHORT);
        superActivityToast.setText(message);
        superActivityToast.show();

    }

}
