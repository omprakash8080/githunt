package com.vsoft.hereyago.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Shubham on 16-Oct-15.
 */
public class InstaremSharedPreferences {

    public interface IE{
        /** Preference file name **/

        public static final String AUTH_TOKEN = "AUTH_TOKEN";

        public static final String countryList = "countryList";

        public static final String MyPREFERENCES = "MyPrefs";

        public static final String authToken = "auth_token";

        public static  final String user_token = "user_token";

//        public static  final String user_loggedIN = "user_loggedIn";

        public static  final String email = "email";

        public static  final String password = "password";

        public static  final String firstName = "first_name";

        public static  final String lastName = "last_name";

        public static  final String mobileNumber = "mobile_number";

        public static  final String dob = "dob";

        public static  final String country = "country";

        public static  final String nationality = "nationality";

        public static  final String employment = "employment_status";

        public static  final String country_pos = "country_pos";

        public static  final String nationality_pos = "nationality_pos";

        public static  final String employment_pos = "employment_status_pos";

        public static  final String refarral_code = "refarral_code";

        public static  final String country_code = "country_code";

        public static  final String nationality_code = "nationality_code";

        public static  final String postal_code = "postal_code";

        public static  final String state = "state";

        public static  final String city = "city";

        public static  final String address1 = "address1";

        public static  final String address2 = "address2";

        public static  final String state_pos = "state_pos";

        public static  final String pep = "pep";

        public static  final String bankaddress1 = "bankaddress1";

        public static  final String bankaddress2 = "bankaddress2";

        public static  final String bankcountry = "bankcountry";

        public static  final String bankstate = "bankstate";

        public static  final String bankname = "bankname";

        public static  final String bank_acc_number = "bankaccnuumber";

        public static  final String ifscCode = "ifscCode";

        public static  final String bankcountry_pos = "bankcountry_pos";

        public static  final String bankstate_pos = "bankstate_pos";

        public static final String profiledatasaved = "profiledatasaved";

        public static final String addressadatasaved = "addressadatasaved";

        public static final String bankdatasaved = "bankdatasaved";

        public static final String documentPending = "document";

        public static final String payeeID = "payeeID";

    }
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    public InstaremSharedPreferences(Context _context) {

        sharedpreferences = _context.getSharedPreferences(IE.MyPREFERENCES,
                Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    public void setPrefrenceStringdata(String key,String value){

        editor.putString(key, value);
        editor.commit();

    }

    public String getPrefrenceStringdata(String key,String default_value){
        return sharedpreferences.getString(key, default_value);
    }

    public void setPrefrenceIntdata(String key,int value){
        editor.putInt(key, value);
        editor.commit();
    }

    public int getPrefrenceIntdata(String key,int default_value){
        return sharedpreferences.getInt(key, default_value);
    }

    public void setPrefrenceBooleandata(String key,boolean value){
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getPrefrenceBooleandata(String key,boolean default_value){
        return sharedpreferences.getBoolean(key, default_value);
    }
}
