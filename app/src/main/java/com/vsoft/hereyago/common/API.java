package com.vsoft.hereyago.common;

/**
 * Created by abc on 15-Oct-15.
 */
public class API {

        public static String BASE_URL = "http://staging.instarem.com/webserviceapi/api";
//    public static String BASE_URL = "https://Api.instarem.com/webserviceapi/api";

    public static String getAuthToken = BASE_URL+"/UserRegistrationsp/APILogin/";

    public static String signUp_url = BASE_URL+"/UserRegistrationsp/GetUserRegistration/";

    public static String logIn_url = BASE_URL+"/UserRegistrationsp/GetLoginDetails/";

    public static String getCity_URL = BASE_URL+"/CommonSP/GetPostCountryCode/";

    public static String profileUpdate_url = BASE_URL+"/UserRegistrationsp/PersonalDetailAdded";

    public static String addressDetails_url = BASE_URL+"/UserRegistrationsp/AddressDetailAdded";

    public static String bankdetail_url = BASE_URL+"/UserRegistrationsp/BankDetail/";

    public static String invite_url = BASE_URL+"/ReferSP/SendInvite/";

    public static String add_payee = BASE_URL+"/PayeeSP/PayeeAdded/";

    public static String referral_detail = BASE_URL+"/ReferSP/GetReferralSummary/";

    public static String invite_list = BASE_URL+"/ReferSP/GetReferralDetail/";

    public static String getProfile_url = BASE_URL+"/UserRegistrationsp/GetProfile/";

    public static String transferWay_url = BASE_URL+"/PaymentSP/TransferWay/";

    public static String social_login_url = BASE_URL+"/UserRegistrationsp/SocialLoginAPI/";

    public static String poli_response_url = BASE_URL+"/PaymentSP/PoliResponce/";

    public static String paymentType_url = BASE_URL+"/PaymentSP/MakePayment/";

}
