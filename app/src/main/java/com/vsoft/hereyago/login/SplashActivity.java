package com.vsoft.hereyago.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.vsoft.hereyago.BaseActivity;
import com.vsoft.hereyago.R;
import com.vsoft.hereyago.Utils.TinyDB;
import com.vsoft.hereyago.api.IOCContainer;
import com.vsoft.hereyago.common.AppConstants;
import com.vsoft.hereyago.common.CommonTasks;
import com.vsoft.hereyago.common.InstaremSharedPreferences;
import com.vsoft.hereyago.main.MainActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import java.util.ArrayList;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends BaseActivity {

    TinyDB tinyDB;
    private ImageView img;
    private ConnectivityManager cm;
    private AlertDialog alertDNew;
    static String TAG = "Splash";

    private InstaremSharedPreferences sp =null;


    @AfterViews
    void init() {

        tinyDB = new TinyDB(getApplicationContext());

        if(!tinyDB.getBoolean("isFirstRun")){

            ArrayList<String> stringList = new ArrayList<>();
            stringList.add("Trending");
            stringList.add("Android");
            stringList.add("Java");
            tinyDB.putListString("tabs", stringList);
            tinyDB.putBoolean("isFirstRun", true);

        }

        IOCContainer.getInstance().init(getApplicationContext());
        sp =  new InstaremSharedPreferences(getApplicationContext());
        img = (ImageView)findViewById(R.id.image);
        DisplayMetrics size = CommonTasks.getScreenSize(SplashActivity.this);
        int height = size.heightPixels;
        int width = size.widthPixels;
        img.getLayoutParams().height = (int) (height * 0.16);
        img.getLayoutParams().width = (int) (width * 0.49);

        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

//            getCountryList();
//            executorService.submit(new Thread(new ThreadGetToken()));
//            executorService.submit(new Thread(new ThreadGetCountryList()));
//            executorService.submit(new Thread(new ThreadGetSourceOfFundId()));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    initMove();
                }
            }).start();
        } else {
            showAlertDialogNew();
        }
    }

    private void initMove() {

        try {
            if(AppConstants.ENV.equalsIgnoreCase("dubug")){
                Thread.sleep(10);//;(3000)//100
            }else {
                Thread.sleep(3000);//;(3000)//100
            }

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

//                            startActivity(new Intent(SplashActivity.this, SettingActivity_.class));

                    startActivity(new Intent(SplashActivity.this, MainActivity_.class));
                    finish();
//                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            });

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void showAlertDialogNew() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("No Network Access");
        builder.setMessage("Do you want to check your internet settings.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                return;
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SplashActivity.this.finish();
                return;
            }
        });
        alertDNew = builder.create();
        alertDNew.show();

    }

    @Override
    public void onSuccess(Object response, String tag) {
        /*if (!TAG.equals(tag)) {
            return;
        }

        if (response instanceof AuthTokenResponse) {
            AuthTokenResponse authTokenResponse =(AuthTokenResponse) response;
            sp.setPrefrenceStringdata(InstaremSharedPreferences.IE.authToken, authTokenResponse.getAuthToken());
        }*/

    }

    @Override
    public void onError(Throwable error, String tag) {

    }

}
