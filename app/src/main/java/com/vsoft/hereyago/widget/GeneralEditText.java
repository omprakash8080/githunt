package com.vsoft.hereyago.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.vsoft.hereyago.R;
import com.vsoft.hereyago.otto.BusProvider;


public class GeneralEditText extends EditText {

    private TextWatcher textwatcher;
    public Drawable imgCloseButton = getResources().getDrawable(
            R.drawable.ic_clear);
    public Drawable searchIcon = getResources().getDrawable(
            R.drawable.ic_search_edit);

    public void handleClearButton() {

        if (getError() != null) {
            setError(null, null);
        }

        if (getText().toString().trim().equals(""))

        {
            // remove clear button

            this.setCompoundDrawables(this.getCompoundDrawables()[0],
                    this.getCompoundDrawables()[1], null,
                    this.getCompoundDrawables()[3]);

        } else

        {
            // add the clear button

            this.setCompoundDrawables(this.getCompoundDrawables()[0],
                    this.getCompoundDrawables()[1], imgCloseButton,
                    this.getCompoundDrawables()[3]);

        }

    }

    public GeneralEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        addListener();
    }

    public GeneralEditText(Context context) {
        super(context);

        addListener();
    }

    public GeneralEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        addListener();
    }

    public void addListener() {

        // formatter.setMaximumFractionDigits(2);
        imgCloseButton.setBounds(0, 0, imgCloseButton.getIntrinsicWidth(),
                imgCloseButton.getIntrinsicHeight());
        addTextChangedListener(textwatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                handleClearButton();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                handleClearButton();
            }
        });

        // text.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        imgCloseButton.setBounds(0, 0, imgCloseButton.getIntrinsicWidth(),
                imgCloseButton.getIntrinsicHeight());

        // There may be initial text in the field, so we may need to display the
        // button

        handleClearButton();

        // if the Close image is displayed and the user remove his finger from
        // the button, clear it. Otherwise do nothing

        this.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (getCompoundDrawables()[2] == null)

                    return false;

                if (event.getAction() != MotionEvent.ACTION_UP)

                    return false;

                if (event.getX() > getWidth() - getPaddingRight()
                        - imgCloseButton.getIntrinsicWidth()) {

                    removeTextChangedListener(textwatcher);

                    setText("");

                    BusProvider.getInstance().post("");

                    addTextChangedListener(textwatcher);
                    handleClearButton();

                }

                return false;

            }
        });
    }

}
