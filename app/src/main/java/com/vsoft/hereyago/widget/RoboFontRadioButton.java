package com.vsoft.hereyago.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;

import com.vsoft.hereyago.MainApplication;
import com.vsoft.hereyago.R;

import java.util.Locale;

public class RoboFontRadioButton extends RadioButton {

	private int checkedTextColor=-1, unCheckedTextColor=-1;

	public RoboFontRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);

		// Typeface.createFromAsset doesn't work in the layout editor.
		// Skipping...
		if (isInEditMode()) {
			return;
		}
		// init();
		setCustomFont(context, attrs);
	}

	private void setCustomFont(Context ctx, AttributeSet attrs) {
		TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.RoboFont);
		String customFont = a.getString(R.styleable.RoboFont_fontName);
		boolean textAllCaps = a.getBoolean(R.styleable.RoboFont_fontAllCaps, false);
		if (textAllCaps) {
			setTransformationMethod(upperCaseTransformation);
		}
		setCustomFont(ctx, customFont);
		a.recycle();
	}

	public void setCustomFont(Context ctx, String asset) {
		Typeface typeface = MainApplication.get(ctx, asset);
		setTypeface(typeface);

	}

	@Override
	public void setChecked(boolean checked) {
		// TODO Auto-generated method stub
		super.setChecked(checked);
	}

	private final TransformationMethod upperCaseTransformation = new TransformationMethod() {

		private final Locale locale = getResources().getConfiguration().locale;

		@Override
		public CharSequence getTransformation(CharSequence source, View view) {
			return source != null ? source.toString().toUpperCase(locale)
					: null;
		}

		@Override
		public void onFocusChanged(View view, CharSequence sourceText,
				boolean focused, int direction, Rect previouslyFocusedRect) {
		}
	};
}
